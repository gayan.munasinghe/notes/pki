# IPTABLES

## Table of Contents

[[_TOC_]]

## Introduction

## Basic iptables

```
sudo iptables -A INPUT -i lo -j ACCEPT
sudo iptables -A INPUT -m conntrack --ctstate ESTABLISHED,RELATED -j ACCEPT
sudo iptables -I INPUT 3 -p udp --dport 1194 -j ACCEPT
sudo iptables -A INPUT -p tcp --dport ssh -j ACCEPT
sudo iptables -A INPUT -j DROP
```

## Editing iptables

```bash
sudo iptables -I INPUT 3 -p udp --dport 1194 -j ACCEPT
```

Before change

```bash
 $ sudo iptables -L -v | head
Chain INPUT (policy ACCEPT 0 packets, 0 bytes)
 pkts bytes target     prot opt in     out     source               destination
66386 8530K f2b-sshd   tcp  --  any    any     anywhere             anywhere             multiport dports ssh
  852 65491 ACCEPT     all  --  lo     any     anywhere             anywhere
65456   28M ACCEPT     all  --  any    any     anywhere             anywhere             ctstate RELATED,ESTABLISHED
 4823  348K ACCEPT     tcp  --  any    any     anywhere             anywhere             tcp dpt:ssh
38170   12M DROP       all  --  any    any     anywhere             anywhere

Chain FORWARD (policy ACCEPT 0 packets, 0 bytes)
 pkts bytes target     prot opt in     out     source               destination
```

After change
```bash
 $ sudo iptables -L -v | head
Chain INPUT (policy ACCEPT 0 packets, 0 bytes)
 pkts bytes target     prot opt in     out     source               destination
66493 8539K f2b-sshd   tcp  --  any    any     anywhere             anywhere             multiport dports ssh
  856 65811 ACCEPT     all  --  lo     any     anywhere             anywhere
    0     0 ACCEPT     udp  --  any    any     anywhere             anywhere             udp dpt:openvpn
65564   28M ACCEPT     all  --  any    any     anywhere             anywhere             ctstate RELATED,ESTABLISHED
 4826  349K ACCEPT     tcp  --  any    any     anywhere             anywhere             tcp dpt:ssh
38245   12M DROP       all  --  any    any     anywhere             anywhere

Chain FORWARD (policy ACCEPT 0 packets, 0 bytes)
```

## Save iptables

```
sudo sh -c "iptables-save -t nat > /etc/iptables.rules"
```

user -t option here to specify the table you want to save.

## Load iptables

```
sudo iptables-restore < /etc/iptables.rules
```

## Check NAT table

```
sudo iptables -L -v -t nat
```

## Cleanup iptables

```
iptables -P INPUT ACCEPT
iptables -P FORWARD ACCEPT
iptables -P OUTPUT ACCEPT
iptables -t nat -Z
iptables -t mangle -Z
iptables -t raw -Z
iptables -Z
iptables -t nat -F
iptables -t mangle -F
iptables -t raw -F
iptables -F
iptables -X
```

## Make iptables persistent

Install iptables-persistent package

```bash
sudo apt install iptables-persistent
```

Save iptables in the following files

```bash
/etc/iptables/rules.v4
/etc/iptables/rules.v6
```

Ex:
```
sudo bash -c 'sudo iptables-save -t filter > /etc/iptables/rules.v4'
```

### Redhat based systems

Install iptables-service

```bash
sudo dnf install iptables-services
```

```bash
sudo systemctl stop firewalld
sudo systemctl disable firewalld
sudo systemctl start iptables
sudo systemctl enable iptables
```

save iptables
```
sudo iptables-save > /etc/sysconfig/iptables
sudo ip6tables-save > /etc/sysconfig/ip6tables
```

```
sudo bash -c "iptables-save -t filter > /etc/sysconfig/iptables"
```
