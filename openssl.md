# OPENSSL

## Table of Contents

[[_TOC_]]

## Introduction

Using openssl to setup a pki and a self signed digital certificate.

## Generate CA

### Generate the root private key of the CA

```bash
openssl genrsa -aes256 -out root-ca-crowdnoetic-private.pem 4096
```

### Generate the public key of the CA

```bash
openssl req -new -x509 -sha256 -days 365 \
    -key root-ca-crowdnoetic-private.pem \
    -out root-ca-crowdnoetic-public.pem
```

### View Certificate

```bash
openssl x509 -in root-ca-crowdnoetic-public.pem -text
```

## Generate CSR

### Create the private key

```bash
openssl genrsa -out gitlab-private.pem 4096
```

### Create a Certificate Signing Request (CSR)

```bash
openssl req -new -sha256 \
    -subj "/CN=Crowdnoetic" \
    -key gitlab-private.pem \
    -out gitlab.csr
```

## Create the Digital certificate

### Create a extfile with all the alternative names

```bash
echo "subjectAltName=DNS:*.crowdnoetic.com,IP:192.168.86.81" >> extfile.cnf
```

### Create the certificate

```bash
openssl x509 -req -sha256 -days 365 -in gitlab.csr \
    -CA root-ca-crowdnoetic-public.pem \
    -CAkey root-ca-crowdnoetic-private.pem \
    -out gitlab.crt \
    -extfile extfile.cnf \
    -CAcreateserial
```

## Install the CA Cert as a trusted root CA

### On Fedora

Move the CA certificate (`root-ca-crowdnoetic-public.pem`) to `/etc/pki/ca-trust/source/anchors/` or `/usr/share/pki/ca-trust-source/anchors/`

Run
```bash
sudo update-ca-trust
```

# GITLAB

Copy the digital certificate and private key to `config/ssl`.

```bash
cp gitlab.crt config/ssl/gitlab.crowdnoetic.com.crt
cp gitlab-private.pem config/ssl/gitlab.crowdnoetic.com.key
```

## Runner

```bash
cp gitlab.crt config/certs/ca.crt
```

# TROUBLESHOOTING

```bash
sudo openssl x509 -in /opt/docker/vault/config/certs/vault.crowdnoetic.com.crt --noout --text
Certificate:
    Data:
        Version: 3 (0x2)
        Serial Number: 4096 (0x1000)
        Signature Algorithm: sha256WithRSAEncryption
        Issuer: C = JP, ST = Tokyo, O = MagentaRoyal, CN = Crowdnoetic CA
        Validity
            Not Before: Feb  1 14:18:09 2023 GMT
            Not After : Feb 11 14:18:09 2024 GMT
        Subject: C = JP, ST = Tokyo, L = Ainokawa, O = MagentaRoyal, CN = gitlab.crowdnoetic.com
        Subject Public Key Info:
            Public Key Algorithm: rsaEncryption
                Public-Key: (2048 bit)
                Modulus:
                    00:e0:c4:bf:1d:c7:24:72:14:c3:64:87:1b:c9:8f:
                    93:14:27:cb:cc:b4:7b:5c:b7:bc:e0:05:62:d4:24:
                    d2:40:8f:83:b8:cf:3e:d7:c2:f4:4a:a5:ed:fc:87:
                    95:8c:31:41:d3:f8:ca:20:c8:28:97:be:a7:98:4b:
                    d9:36:a6:1d:84:2d:d5:22:78:d7:b0:87:82:54:a9:
                    da:52:51:a8:45:37:c8:6f:17:ad:7d:03:07:bc:2c:
                    54:c9:58:f2:ec:97:f4:e7:d0:40:a4:45:49:b7:6e:
                    66:cf:9b:c8:05:e4:e3:6f:80:0d:25:a2:3a:87:c7:
                    2e:61:2d:b6:b7:e7:1c:bb:01:e2:f1:ca:3c:5f:3e:
                    94:f6:e2:5a:b2:21:5b:dd:5c:33:ef:42:bf:7e:95:
                    f2:20:2a:a9:f8:cb:ae:39:96:d9:cc:88:88:15:52:
                    c6:9d:07:75:91:44:6a:8a:86:60:1f:49:e5:aa:c2:
                    c6:ee:eb:80:1d:fb:f3:fa:05:ca:bd:2b:9c:06:3c:
                    ef:2b:50:96:8a:50:fc:94:dc:e5:0d:d6:60:86:b2:
                    89:a0:21:0f:49:1d:9a:ba:2d:3a:98:4c:3c:4d:ca:
                    a7:8d:d7:ab:26:22:dd:b6:95:bc:30:5f:fb:5e:f3:
                    49:62:79:9b:c0:db:40:a7:34:80:57:66:00:8a:94:
                    17:1d
                Exponent: 65537 (0x10001)
        X509v3 extensions:
            X509v3 Subject Alternative Name: 
                DNS:*.crowdnoetic.com, IP Address:192.168.86.81
    Signature Algorithm: sha256WithRSAEncryption
    Signature Value:
        a4:87:6f:3a:38:d6:12:71:57:d2:ad:18:2a:b5:19:18:5b:04:
        a4:65:82:57:d4:52:cc:70:81:a7:88:c3:d1:dd:6d:96:56:be:
        d0:43:fd:97:a2:ab:61:21:98:f5:1c:b8:cd:02:ca:69:c8:20:
        5f:50:59:d0:b8:96:2c:41:fe:c3:04:29:f9:01:a4:9c:88:04:
        5a:33:df:0a:f8:6d:5c:b2:4a:a5:a1:67:87:78:3b:89:42:cc:
        85:44:3e:06:58:f9:bf:6b:ab:d4:d1:2b:9d:fd:bd:2c:eb:08:
        37:0a:83:1f:9c:9f:2c:24:8b:d3:2b:e1:46:5f:36:f3:1e:5a:
        d7:0f:b9:85:58:94:ee:bd:57:c2:77:d6:38:d3:d5:e0:18:1b:
        76:08:17:da:b7:37:6c:47:3c:5b:16:12:b2:e0:ee:c8:e2:57:
        40:09:71:d3:93:b8:66:1a:c8:e1:b3:ff:96:fd:52:7c:54:40:
        91:aa:f7:12:ae:a3:6e:45:02:0b:fa:f3:dc:fa:a7:e7:5c:25:
        6a:ce:88:ff:33:67:6f:9d:3c:1f:7f:03:d2:8f:1a:8c:4e:cf:
        8a:e3:bd:78:d6:21:80:e1:26:a4:a5:23:4f:ea:96:71:b6:d8:
        66:36:da:4e:5e:57:42:eb:46:5d:e2:ac:67:f0:39:93:02:60:
        71:f8:5c:8e:c8:78:71:a9:d3:21:52:f1:df:b1:ae:0c:0a:84:
        c8:19:2e:ef:97:7b:0e:fa:2d:2d:36:5e:ca:b1:5e:10:2e:33:
        e8:72:3d:01:bb:5e:7f:6f:8d:6e:5e:ae:9d:10:8a:bf:c3:13:
        d4:4e:82:96:75:3c:ae:b8:06:0e:6d:e8:53:3d:23:61:b7:a7:
        48:e2:92:9a:6a:6f:16:c5:bb:89:5b:d1:9c:de:20:d5:36:2a:
        45:3f:e1:c8:94:90:38:a1:d4:f4:a8:c1:bf:21:c0:65:03:f1:
        1c:ba:1a:63:a8:9b:64:66:01:37:a0:56:bc:11:8e:86:d8:ed:
        64:68:5b:24:6d:e9:ab:d2:77:f7:73:5c:f7:cf:c2:8d:59:69:
        1a:1b:41:bf:97:2c:fb:db:e6:9d:00:8c:d3:d9:2a:f6:5b:e5:
        02:01:33:3f:d8:43:23:e5:8a:20:66:44:f1:fc:11:26:f4:de:
        10:7a:7f:b8:bf:95:a9:53:03:50:8c:82:b9:b1:3e:9f:28:2f:
        65:18:6a:1d:77:06:e4:f9:2c:e4:3d:ad:d7:fe:a3:63:7b:bd:
        18:a4:ee:a5:2b:06:f7:1d:7d:57:fd:54:7f:25:41:6d:4f:9f:
        c3:81:fa:30:d6:e2:d1:8f:57:6d:e1:05:bf:59:3a:1a:a6:00:
        e5:49:e4:9d:0d:9a:c6:ac
```
