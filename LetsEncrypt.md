# Let's Encrypt

## Table of Contents

[[_TOC_]]

## Introduction

Let's Encrypt provides X.509 certificates for TLS at no charge.

## Setting up a certificate for redash server with nginx as a reverse-proxy

redash docker-compose file

```
version: "2"
x-redash-service: &redash-service
  image: redash/redash:8.0.0.b32245
  depends_on:
    - postgres
    - redis
  env_file: /opt/docker/redash/env
  restart: always
  networks:
    - default
  #extra_hosts:
  #  - "host.docker.internal:host-gateway"
services:
  server:
    <<: *redash-service
    command: server
    #ports:
    #        - "127.0.0.1:5000:5000"
    environment:
      REDASH_WEB_WORKERS: 4
  scheduler:
    <<: *redash-service
    command: scheduler
    environment:
      QUEUES: "celery"
      WORKERS_COUNT: 1
  scheduled_worker:
    <<: *redash-service
    command: worker
    environment:
      QUEUES: "scheduled_queries,schemas"
      WORKERS_COUNT: 1
  adhoc_worker:
    <<: *redash-service
    command: worker
    environment:
      QUEUES: "queries"
      WORKERS_COUNT: 2
  redis:
    image: redis:5.0-alpine
    restart: always
  postgres:
    image: postgres:9.6-alpine
    env_file: /opt/docker/redash/env
    volumes:
      - /opt/docker/redash/postgres-data:/var/lib/postgresql/data
    restart: always
  nginx:
    image: redash/nginx:latest
    ports:
            - "80:80"
            - "443:443"
    depends_on:
      - server
    links:
      - server:redash
    volumes:
      - /opt/docker/redash/nginx/nginx.conf:/etc/nginx/conf.d/default.conf
      - /opt/docker/redash/nginx/certs:/etc/letsencrypt
      - /opt/docker/redash/nginx/certs-data:/data/letsencrypt
    restart: always
networks:
  default:
    ipam:
      config:
        - subnet: 172.18.0.0/16
    driver_opts:
      com.docker.network.bridge.name: docker1
```

## Steps to setup https

source:
https://gist.github.com/arikfr/64c9ff8d2f2b703d4e44fe9e45a7730e

1. Make sure the domain you picked points at the IP of your Redash server.
2. Switch to the root user (sudo su).
3. Create a folder named nginx in /opt/redash.
4. Create in the nginx folder two additional folders: certs and certs-data.
5. Create the file /opt/redash/nginx/nginx.conf and place the following in it: (replace example.redashapp.com with your domain name)

```
upstream redash {
    server redash:5000;
}

server {
    listen      80;
    listen [::]:80;
    server_name example.redashapp.com;

    location ^~ /ping {
        proxy_set_header Host $http_host;
        proxy_set_header X-Real-IP $remote_addr;
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
        proxy_set_header X-Forwarded-Proto $http_x_forwarded_proto;

        proxy_pass       http://redash;
    }

    location / {
        rewrite ^ https://$host$request_uri? permanent;
    }

    location ^~ /.well-known {
        allow all;
        root  /data/letsencrypt/;
    }
}
```

6. Edit /opt/redash/docker-compose.yml and update the nginx service to look like the following:

```
nginx:
 image: nginx:latest
 ports:
   - "80:80"
   - "443:443"
 depends_on:
   - server
 links:
   - server:redash
 volumes:
   - /opt/redash/nginx/nginx.conf:/etc/nginx/conf.d/default.conf
   - /opt/redash/nginx/certs:/etc/letsencrypt
   - /opt/redash/nginx/certs-data:/data/letsencrypt
 restart: always
```

7. Update Docker Compose: docker-compose up -d.
8. Generate certificates: (remember to change the domain name)

```
docker run -it --rm \
   -v /opt/redash/nginx/certs:/etc/letsencrypt \
   -v /opt/redash/nginx/certs-data:/data/letsencrypt \
   certbot/certbot \
   certonly \
   --webroot --webroot-path=/data/letsencrypt \
   -d example.redashapp.com
```

9. Assuming the previous step was succesful, update the nginx config to include the SSL configuration

```
upstream redash {
    server redash:5000;
}

server {
    listen      80;
    listen [::]:80;
    server_name example.redashapp.com;

    location ^~ /ping {
        proxy_set_header Host $http_host;
        proxy_set_header X-Real-IP $remote_addr;
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
        proxy_set_header X-Forwarded-Proto $scheme;

        proxy_pass       http://redash;
    }

    location / {
        rewrite ^ https://$host$request_uri? permanent;
    }

    location ^~ /.well-known {
        allow all;
        root  /data/letsencrypt/;
    }
}

server {
 listen      443           ssl http2;
 listen [::]:443           ssl http2;
 server_name               example.redashapp.com;

 add_header                Strict-Transport-Security "max-age=31536000" always;

 ssl_session_cache         shared:SSL:20m;
 ssl_session_timeout       10m;

 ssl_protocols             TLSv1 TLSv1.1 TLSv1.2;
 ssl_prefer_server_ciphers on;
 ssl_ciphers               "ECDH+AESGCM:ECDH+AES256:ECDH+AES128:!ADH:!AECDH:!MD5;";

 ssl_stapling              on;
 ssl_stapling_verify       on;
 resolver                  8.8.8.8 8.8.4.4;

 ssl_certificate           /etc/letsencrypt/live/example.redashapp.com/fullchain.pem;
 ssl_certificate_key       /etc/letsencrypt/live/example.redashapp.com/privkey.pem;
 ssl_trusted_certificate   /etc/letsencrypt/live/example.redashapp.com/chain.pem;

 access_log                /dev/stdout;
 error_log                 /dev/stderr info;

 # other configs

 location / {
     proxy_set_header Host $http_host;
     proxy_set_header X-Real-IP $remote_addr;
     proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
     proxy_set_header X-Forwarded-Proto $scheme;

     proxy_pass       http://redash;
 }
}
```

10. Restart nginx: docker-compose restart nginx.
11. All done, your Redash instance should be available via HTTPS now. clap

To renew the certificate in the future, you can use the following command:

```
docker run -t --rm -v \
/opt/redash/nginx/certs:/etc/letsencrypt \
-v /opt/redash/nginx/certs-data:/data/letsencrypt certbot/certbot \
renew --webroot --webroot-path=/data/letsencrypt
```
