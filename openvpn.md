# OPENVPN

## Table of Contents

[[_TOC_]]

## Introduction

To setup a vpn using open vpn, you need to setup a PKI. easy-rsa can be set to set up pki.
In this setup CA resides in a separate server. Both openvpn server and client will generate
certficate sigining requests to get their certficates signed with the CA.


## Setup CA

### Clone easy-rsa

```bash
$ git clone https://github.com/OpenVPN/easy-rsa.git
```

### Initialize PKI

``` bash
./easyrsa ini-pki

* Notice: init-pki complete; you may now create a CA or requests.
Your newly created PKI dir is:
* /home/gayan/work/github/easy-rsa/easyrsa3/pki
* Notice: * Easy-RSA 'vars' file has now been moved to your PKI above.

```

### Build CA

``` bash
$ ./easyrsa build-ca
* Notice: Note: using Easy-RSA configuration from: /home/gayan/work/github/easy-rsa/easyrsa3/pki/vars
* Notice: Using SSL: openssl OpenSSL 1.1.1g FIPS  21 Apr 2020

Enter New CA Key Passphrase:
Re-Enter New CA Key Passphrase:
Generating RSA private key, 2048 bit long modulus (2 primes)
.....................+++++
....+++++
e is 65537 (0x010001)
You are about to be asked to enter information that will be incorporated
into your certificate request.
What you are about to enter is what is called a Distinguished Name or a DN.
There are quite a few fields but you can leave some blank
For some fields there will be a default value,
If you enter '.', the field will be left blank.
-----
Common Name (eg: your user, host, or server name) [Easy-RSA CA]:quantsregent
* Notice: CA creation complete and you may now import and sign cert requests.
Your new CA certificate file for publishing is at:
/home/gayan/work/github/easy-rsa/easyrsa3/pki/ca.crt

```

CA's private key will be created in `pki/private` directory.

CA's root certificate should be copied to both server and client.

```bash
ll pki/private/
total 4
-rw-------. 1 gayan gayan 1766 Apr  8 10:27 ca.key
```

## Setup OpenVPN Server

### Initialize PKI
```bash
./easyrsa init-pki
```

### Generate a Certificate Signing Request

```bash
 $ ./easyrsa gen-req linode nopass
Using SSL: openssl OpenSSL 1.1.1k  FIPS 25 Mar 2021
Generating a RSA private key
................................+++++
...........+++++
writing new private key to '/home/gayan/easy-rsa/easyrsa3/pki/easy-rsa-208790.7jFefK/tmp.Mlm603'
-----
You are about to be asked to enter information that will be incorporated
into your certificate request.
What you are about to enter is what is called a Distinguished Name or a DN.
There are quite a few fields but you can leave some blank
For some fields there will be a default value,
If you enter '.', the field will be left blank.
-----
Common Name (eg: your user, host, or server name) [linode]:

Keypair and certificate request completed. Your files are:
req: /home/gayan/easy-rsa/easyrsa3/pki/reqs/linode.req
key: /home/gayan/easy-rsa/easyrsa3/pki/private/linode.key
```
CSR should be copied to your CA node.
It should be imported first before sigining.

#### Import request

```bash
$ ./easyrsa import-req linode.req linode
* Notice: Note: using Easy-RSA configuration from: /home/gayan/work/github/easy-rsa/easyrsa3/pki/vars
* Notice: Using SSL: openssl OpenSSL 1.1.1g FIPS  21 Apr 2020
* Notice: The request has been successfully imported with a short name of: linode
You may now use this name to perform signing operations on this request.

```

#### Sign request

```bash
./easyrsa sign-req server linode
* Notice: Note: using Easy-RSA configuration from: /home/gayan/work/github/easy-rsa/easyrsa3/pki/vars
* Notice: Using SSL: openssl OpenSSL 1.1.1g FIPS  21 Apr 2020


You are about to sign the following certificate.
Please check over the details shown below for accuracy. Note that this request
has not been cryptographically verified. Please be sure it came from a trusted
source or that you have verified the request checksum with the sender.

Request subject, to be signed as a server certificate for 825 days:

subject=
    commonName                = linode


Type the word 'yes' to continue, or any other input to abort.
  Confirm request details: yes
Using configuration from /home/gayan/work/github/easy-rsa/easyrsa3/pki/easy-rsa-3460958.eFsGfz/tmp.4f5c97
Enter pass phrase for /home/gayan/work/github/easy-rsa/easyrsa3/pki/private/ca.key:
Check that the request matches the signature
Signature ok
The Subject's Distinguished Name is as follows
commonName            :ASN.1 12:'linode'
Certificate is to be certified until Jul 11 03:23:27 2024 GMT (825 days)

Write out database with 1 new entries
Data Base Updated
* Notice: Certificate created at: /home/gayan/work/github/easy-rsa/easyrsa3/pki/issued/linode.crt


```

### Generate Diffie Hellman Key

```bash
 $ ./easyrsa gen-dh
Using SSL: openssl OpenSSL 1.1.1k  FIPS 25 Mar 2021
Generating DH parameters, 2048 bit long safe prime, generator 2
This is going to take a long time
.....................................................................+....+...................................................++*++*++*++*

DH parameters of size 2048 created at /home/gayan/easy-rsa/easyrsa3/pki/dh.pem

```
### Install openvpn

```
sudo yum install openvpn
```

### Copy CA's certificate to server

### Add Server config file.

A sample configuration file can be copied from openvpn sample configuration files.

```bash
cp /usr/share/doc/openvpn/sample/sample-config-files/server.conf .
```

```bash
$ grep ^[^#\;] server.conf
port 1194
proto udp
dev tun
ca ca.crt
cert linode.crt
key private/linode.key  # This file should be kept secret
dh dh.pem
topology subnet
server 10.7.0.0 255.255.255.0
ifconfig-pool-persist ipp.txt
keepalive 10 120
cipher AES-256-CBC
user nobody
group nobody
persist-key
persist-tun
status openvpn-status.log
verb 3
explicit-exit-notify 1
```

### Run Server

```
 $ sudo openvpn server.conf
Fri Apr  8 09:53:07 2022 OpenVPN 2.4.12 x86_64-redhat-linux-gnu [SSL (OpenSSL)] [LZO] [LZ4] [EPOLL] [PKCS11] [MH/PKTINFO] [AEAD] built on Mar 17 2022
Fri Apr  8 09:53:07 2022 library versions: OpenSSL 1.1.1k  FIPS 25 Mar 2021, LZO 2.08
Fri Apr  8 09:53:07 2022 Diffie-Hellman initialized with 2048 bit key
Fri Apr  8 09:53:07 2022 TUN/TAP device tun0 opened
Fri Apr  8 09:53:07 2022 TUN/TAP TX queue length set to 100
Fri Apr  8 09:53:07 2022 /sbin/ip link set dev tun0 up mtu 1500
Fri Apr  8 09:53:07 2022 /sbin/ip addr add dev tun0 10.7.0.1/24 broadcast 10.7.0.255
Fri Apr  8 09:53:07 2022 Could not determine IPv4/IPv6 protocol. Using AF_INET
Fri Apr  8 09:53:07 2022 Socket Buffers: R=[212992->212992] S=[212992->212992]
Fri Apr  8 09:53:07 2022 UDPv4 link local (bound): [AF_INET][undef]:1194
Fri Apr  8 09:53:07 2022 UDPv4 link remote: [AF_UNSPEC]
Fri Apr  8 09:53:07 2022 GID set to nobody
Fri Apr  8 09:53:07 2022 UID set to nobody
Fri Apr  8 09:53:07 2022 MULTI: multi_init called, r=256 v=256
Fri Apr  8 09:53:07 2022 IFCONFIG POOL: base=10.7.0.2 size=252, ipv6=0
Fri Apr  8 09:53:07 2022 IFCONFIG POOL LIST
Fri Apr  8 09:53:07 2022 Initialization Sequence Completed

```


## Setup client

### Initialize PKI

```bash
./easyrsa init-pki
```

### Generate Certificate Siging Request(CSR)

``` bash
./easyrsa gen-req worker nopass
* Notice: Note: using Easy-RSA configuration from: /home/gayan/Downloads/easy-rsa/easyrsa3/pki/vars
* Notice: Using SSL: openssl OpenSSL 1.1.1g FIPS  21 Apr 2020
Generating a RSA private key
.............................+++++
.................+++++
writing new private key to '/home/gayan/Downloads/easy-rsa/easyrsa3/pki/easy-rsa-3827949.lK9dOB/tmp.04c170'
-----
You are about to be asked to enter information that will be incorporated
into your certificate request.
What you are about to enter is what is called a Distinguished Name or a DN.
There are quite a few fields but you can leave some blank
For some fields there will be a default value,
If you enter '.', the field will be left blank.
-----
Common Name (eg: your user, host, or server name) [worker]:
* Notice: Keypair and certificate request completed. Your files are:
req: /home/gayan/Downloads/easy-rsa/easyrsa3/pki/reqs/worker.req
key: /home/gayan/Downloads/easy-rsa/easyrsa3/pki/private/worker.key
```

### Import CSR
CSR should be copied to CA node for sigining.

```
./easyrsa import-req worker.req worker
* Notice: Note: using Easy-RSA configuration from: /home/gayan/work/github/easy-rsa/easyrsa3/pki/vars
* Notice: Using SSL: openssl OpenSSL 1.1.1g FIPS  21 Apr 2020
* Notice: The request has been successfully imported with a short name of: worker
You may now use this name to perform signing operations on this request.

```

### Sign CSR

```bash
./easyrsa sign-req client worker
* Notice: Note: using Easy-RSA configuration from: /home/gayan/work/github/easy-rsa/easyrsa3/pki/vars
* Notice: Using SSL: openssl OpenSSL 1.1.1g FIPS  21 Apr 2020


You are about to sign the following certificate.
Please check over the details shown below for accuracy. Note that this request
has not been cryptographically verified. Please be sure it came from a trusted
source or that you have verified the request checksum with the sender.

Request subject, to be signed as a client certificate for 825 days:

subject=
    commonName                = worker


Type the word 'yes' to continue, or any other input to abort.
  Confirm request details: yes
Using configuration from /home/gayan/work/github/easy-rsa/easyrsa3/pki/easy-rsa-3834289.509AC3/tmp.935a6d
Enter pass phrase for /home/gayan/work/github/easy-rsa/easyrsa3/pki/private/ca.key:
Check that the request matches the signature
Signature ok
The Subject's Distinguished Name is as follows
commonName            :ASN.1 12:'worker'
Certificate is to be certified until Jul 11 10:04:36 2024 GMT (825 days)

Write out database with 1 new entries
Data Base Updated
* Notice: Certificate created at: /home/gayan/work/github/easy-rsa/easyrsa3/pki/issued/worker.crt
```

### Install openvpn

```
sudo yum install openvpn
```

### Copy CA's certificate to client

### Add Client config file.

A sample configuration file can be copied from openvpn sample configuration files.

```bash
cp /usr/share/doc/openvpn/sample/sample-config-files/client.conf .
```

```bash
grep ^[^#\;] client.conf
client
dev tun
proto udp
remote 170.187.151.84 1194
resolv-retry infinite
nobind
user nobody
group nobody
persist-key
persist-tun
ca ca.crt
cert worker.crt
key private/worker.key
remote-cert-tls server
cipher AES-256-CBC
verb 3
```

### Run Client

```bash
sudo openvpn client.conf
Fri Apr  8 19:10:52 2022 OpenVPN 2.4.11 x86_64-redhat-linux-gnu [SSL (OpenSSL)] [LZO] [LZ4] [EPOLL] [PKCS11] [MH/PKTINFO] [AEAD] built on Apr 21 2021
Fri Apr  8 19:10:52 2022 library versions: OpenSSL 1.1.1g FIPS  21 Apr 2020, LZO 2.08
Fri Apr  8 19:10:52 2022 TCP/UDP: Preserving recently used remote address: [AF_INET]170.187.151.84:1194
Fri Apr  8 19:10:52 2022 Socket Buffers: R=[212992->212992] S=[212992->212992]
Fri Apr  8 19:10:52 2022 UDP link local: (not bound)
Fri Apr  8 19:10:52 2022 UDP link remote: [AF_INET]170.187.151.84:1194
Fri Apr  8 19:10:52 2022 NOTE: UID/GID downgrade will be delayed because of --client, --pull, or --up-delay
Fri Apr  8 19:10:52 2022 TLS: Initial packet from [AF_INET]170.187.151.84:1194, sid=5eaf82b4 68901a0f
Fri Apr  8 19:10:52 2022 VERIFY OK: depth=1, CN=quantsregent
Fri Apr  8 19:10:52 2022 VERIFY KU OK
Fri Apr  8 19:10:52 2022 Validating certificate extended key usage
Fri Apr  8 19:10:52 2022 ++ Certificate has EKU (str) TLS Web Server Authentication, expects TLS Web Server Authentication
Fri Apr  8 19:10:52 2022 VERIFY EKU OK
Fri Apr  8 19:10:52 2022 VERIFY OK: depth=0, CN=linode
Fri Apr  8 19:10:52 2022 Control Channel: TLSv1.3, cipher TLSv1.3 TLS_AES_256_GCM_SHA384, 2048 bit RSA
Fri Apr  8 19:10:52 2022 [linode] Peer Connection Initiated with [AF_INET]170.187.151.84:1194
Fri Apr  8 19:10:54 2022 SENT CONTROL [linode]: 'PUSH_REQUEST' (status=1)
Fri Apr  8 19:10:54 2022 PUSH: Received control message: 'PUSH_REPLY,route-gateway 10.7.0.1,topology subnet,ping 10,ping-restart 120,ifconfig 10.7.0.2 255.255.255.0,peer-id 0,cipher AES-256-GCM'
Fri Apr  8 19:10:54 2022 OPTIONS IMPORT: timers and/or timeouts modified
Fri Apr  8 19:10:54 2022 OPTIONS IMPORT: --ifconfig/up options modified
Fri Apr  8 19:10:54 2022 OPTIONS IMPORT: route-related options modified
Fri Apr  8 19:10:54 2022 OPTIONS IMPORT: peer-id set
Fri Apr  8 19:10:54 2022 OPTIONS IMPORT: adjusting link_mtu to 1624
Fri Apr  8 19:10:54 2022 OPTIONS IMPORT: data channel crypto options modified
Fri Apr  8 19:10:54 2022 Data Channel: using negotiated cipher 'AES-256-GCM'
Fri Apr  8 19:10:54 2022 Outgoing Data Channel: Cipher 'AES-256-GCM' initialized with 256 bit key
Fri Apr  8 19:10:54 2022 Incoming Data Channel: Cipher 'AES-256-GCM' initialized with 256 bit key
Fri Apr  8 19:10:54 2022 TUN/TAP device tun0 opened
Fri Apr  8 19:10:54 2022 TUN/TAP TX queue length set to 100
Fri Apr  8 19:10:54 2022 /sbin/ip link set dev tun0 up mtu 1500
Fri Apr  8 19:10:54 2022 /sbin/ip addr add dev tun0 10.7.0.2/24 broadcast 10.7.0.255
Fri Apr  8 19:10:54 2022 GID set to nobody
Fri Apr  8 19:10:54 2022 UID set to nobody
Fri Apr  8 19:10:54 2022 WARNING: this configuration may cache passwords in memory -- use the auth-nocache option to prevent this
Fri Apr  8 19:10:54 2022 Initialization Sequence Completed

```

### Server logs when client connects

```bash
Fri Apr  8 10:10:52 2022 150.249.168.51:51926 TLS: Initial packet from [AF_INET]150.249.168.51:51926, sid=93d4d18a 2e90a8d8
Fri Apr  8 10:10:52 2022 150.249.168.51:51926 VERIFY OK: depth=1, CN=quantsregent
Fri Apr  8 10:10:52 2022 150.249.168.51:51926 VERIFY OK: depth=0, CN=worker
Fri Apr  8 10:10:52 2022 150.249.168.51:51926 peer info: IV_VER=2.4.11
Fri Apr  8 10:10:52 2022 150.249.168.51:51926 peer info: IV_PLAT=linux
Fri Apr  8 10:10:52 2022 150.249.168.51:51926 peer info: IV_PROTO=2
Fri Apr  8 10:10:52 2022 150.249.168.51:51926 peer info: IV_NCP=2
Fri Apr  8 10:10:52 2022 150.249.168.51:51926 peer info: IV_CIPHERS=AES-256-GCM:AES-128-GCM:AES-256-CBC
Fri Apr  8 10:10:52 2022 150.249.168.51:51926 peer info: IV_LZ4=1
Fri Apr  8 10:10:52 2022 150.249.168.51:51926 peer info: IV_LZ4v2=1
Fri Apr  8 10:10:52 2022 150.249.168.51:51926 peer info: IV_LZO=1
Fri Apr  8 10:10:52 2022 150.249.168.51:51926 peer info: IV_COMP_STUB=1
Fri Apr  8 10:10:52 2022 150.249.168.51:51926 peer info: IV_COMP_STUBv2=1
Fri Apr  8 10:10:52 2022 150.249.168.51:51926 peer info: IV_TCPNL=1
Fri Apr  8 10:10:53 2022 150.249.168.51:51926 Control Channel: TLSv1.3, cipher TLSv1.3 TLS_AES_256_GCM_SHA384, 2048 bit RSA
Fri Apr  8 10:10:53 2022 150.249.168.51:51926 [worker] Peer Connection Initiated with [AF_INET]150.249.168.51:51926
Fri Apr  8 10:10:53 2022 worker/150.249.168.51:51926 MULTI_sva: pool returned IPv4=10.7.0.2, IPv6=(Not enabled)
Fri Apr  8 10:10:53 2022 worker/150.249.168.51:51926 MULTI: Learn: 10.7.0.2 -> worker/150.249.168.51:51926
Fri Apr  8 10:10:53 2022 worker/150.249.168.51:51926 MULTI: primary virtual IP for worker/150.249.168.51:51926: 10.7.0.2
Fri Apr  8 10:10:54 2022 worker/150.249.168.51:51926 PUSH: Received control message: 'PUSH_REQUEST'
Fri Apr  8 10:10:54 2022 worker/150.249.168.51:51926 SENT CONTROL [worker]: 'PUSH_REPLY,route-gateway 10.7.0.1,topology subnet,ping 10,ping-restart 120,ifconfig 10.7.0.2 255.255.255.0,peer-id 0,cipher AES-256-GCM' (status=1)
Fri Apr  8 10:10:54 2022 worker/150.249.168.51:51926 Data Channel: using negotiated cipher 'AES-256-GCM'
Fri Apr  8 10:10:54 2022 worker/150.249.168.51:51926 Outgoing Data Channel: Cipher 'AES-256-GCM' initialized with 256 bit key
Fri Apr  8 10:10:54 2022 worker/150.249.168.51:51926 Incoming Data Channel: Cipher 'AES-256-GCM' initialized with 256 bit key

```

## Additional Security Meassures

### tls-auth

Additional security can be added using the tls-auth directive.

#### Generate key

```bash
openvpn --genkey --secret ta.key
```

This key should be copied to both server and client via an already available secure channel.

#### Add this to server configuration

```bash
grep tls-auth server.conf
tls-auth ta.key 0 # This file is secret
```

#### Add to client configuration

```bash
grep ta.key client.conf
tls-auth ta.key 1
```
---

### Use udp

```
proto udp
```

### user/group

```
user nobody
group nobody
```

note: In debian based systems you might need to use the group `nogroup`.

### Larger RSA keys

Set KEY_SIZE to 2048

```bash
grep KEY_SIZE pki/vars
#set_var EASYRSA_KEY_SIZE       2048
```

### Use Larger symmetic keys

```
cipher AES-256-CBC
```

## iptables

### server

/usr/sbin/iptables -t nat -A POSTROUTING -s 10.8.0.0/24 ! -d 10.8.0.0/24 -j SNAT --to 89.233.104.47 (code=exited, status=0/SUCCESS)
/usr/sbin/iptables -I INPUT -p udp --dport 1194 -j ACCEPT (code=exited, status=0/SUCCESS)
/usr/sbin/iptables -I FORWARD -s 10.8.0.0/24 -j ACCEPT (code=exited, status=0/SUCCESS)
/usr/sbin/iptables -I FORWARD -m state --state RELATED,ESTABLISHED -j ACCEPT (code=exited, status=0/SUCCESS)
/usr/sbin/ip6tables -t nat -A POSTROUTING -s fddd:1194:1194:1194::/64 ! -d fddd:1194:1194:1194::/64 -j SNAT --to 2602:ff16:8:0:1:aa:0:1 (code=exited, status=0/SUCCESS)
/usr/sbin/ip6tables -I FORWARD -s fddd:1194:1194:1194::/64 -j ACCEPT (code=exited, status=0/SUCCESS)
/usr/sbin/ip6tables -I FORWARD -m state --state RELATED,ESTABLISHED -j ACCEPT (code=exited, status=0/SUCCESS)

## Add service

```
[Unit]
Description=openvpn-server ssdnode service

[Service]
ExecStart=/usr/sbin/openvpn /etc/openvpn/server/server.conf

[Install]
WantedBy=multi-user.target
 ```

### Readhat
 ```
sudo ln -sf /etc/openvpn/client/openvpn.client.service /etc/systemd/system/multi-user.target.wants/openvpn.client.service
 ```

### Debian
 ```
sudo ln -sf /etc/openvpn/client/openvpn.client.service /etc/systemd/system/openvpn.client.service
 ```

### SELinux

You might run into permission errors.

```
type=AVC msg=audit(1649802375.685:4296): avc:  denied  { read } for  pid=1 comm="systemd" name="openvpn.client.service" dev="sda" ino=265972 scontext=system_u:system_r:init_t:s0 tcontext=unconfined_u:object_r:user_home_t:s0 tclass=file permissive=0
```

SELinux context should be like this.

```
 $ ls -alZ
total 36
drwxr-x---. 2 gayan gayan system_u:object_r:openvpn_etc_t:s0     4096 Apr 12 22:24 .
drwxr-xr-x. 4 root  root  system_u:object_r:openvpn_etc_t:s0     4096 Apr 12 22:16 ..
-rw-------. 1 gayan gayan unconfined_u:object_r:openvpn_etc_t:s0 1204 Apr  9 23:36 ca.crt
-rw-r--r--. 1 gayan gayan unconfined_u:object_r:openvpn_etc_t:s0 3695 Apr 12 22:24 client.conf
-rw-------. 1 gayan gayan unconfined_u:object_r:openvpn_etc_t:s0 4528 Apr 12 22:00 openvpn.client.linode.crt
-rw-------. 1 gayan gayan unconfined_u:object_r:openvpn_etc_t:s0 1704 Apr 12 21:56 openvpn.client.linode.key
-rw-r--r--. 1 gayan gayan unconfined_u:object_r:openvpn_etc_t:s0  151 Apr 11 14:09 openvpn.client.service
-rw-------. 1 gayan gayan unconfined_u:object_r:openvpn_etc_t:s0  636 Apr 11 14:43 ta.key

```

Change it if it is different.

```
 $ ls -alZ /etc/openvpn/client/
total 36
drwxr-x---. 2 gayan gayan system_u:object_r:openvpn_etc_t:s0     4096 Apr 12 22:24 .
drwxr-xr-x. 4 root  root  system_u:object_r:openvpn_etc_t:s0     4096 Apr 12 22:16 ..
-rw-------. 1 gayan gayan unconfined_u:object_r:user_home_t:s0   1204 Apr  9 23:36 ca.crt
-rw-r--r--. 1 gayan gayan unconfined_u:object_r:openvpn_etc_t:s0 3695 Apr 12 22:24 client.conf
-rw-------. 1 gayan gayan unconfined_u:object_r:user_home_t:s0   4528 Apr 12 22:00 openvpn.client.linode.crt
-rw-------. 1 gayan gayan unconfined_u:object_r:etc_t:s0         1704 Apr 12 21:56 openvpn.client.linode.key
-rw-r--r--. 1 gayan gayan unconfined_u:object_r:user_home_t:s0    151 Apr 11 14:09 openvpn.client.service
-rw-------. 1 gayan gayan unconfined_u:object_r:user_home_t:s0    636 Apr 11 14:43 ta.key

 $ sudo chcon -t openvpn_etc_t ca.crt

```

## Add static ips to clients

Add the following line to server

```
client-config-dir /etc/openvpn/server/ccd
```

Get the client CN

```
sudo openssl x509 -subject -noout -in openvpn.client.worker.crt
subject=CN = openvpn.client.worker
```

Add a file in the client-config-dir

```
cat openvpn.client.worker
ifconfig-push 10.8.0.10 255.255.255.0
```
## Troubleshooting

By default easy_rsa will set the Next Update date for 30 days. This cannot be changed.

```
sudo openssl crl -in /etc/openvpn/taskmaster/crl.pem --noout -text
Certificate Revocation List (CRL):
        Version 2 (0x1)
        Signature Algorithm: sha512WithRSAEncryption
        Issuer: C = JP, ST = CB, L = Ainokawa, O = quantsregent, OU = My Organizational Unit, CN = ChangeMe, emailAddress = admin@quantsregent.com
        Last Update: Apr 29 13:29:30 2022 GMT
        *Next Update: May 29 13:29:30 2022 GMT*
        CRL extensions:
            X509v3 Authority Key Identifier:
                keyid:65:FD:57:B8:38:D3:8B:44:0E:B2:3C:9E:EC:B3:92:5F:EA:87:58:F4
                DirName:/C=JP/ST=CB/L=Ainokawa/O=quantsregent/OU=My Organizational Unit/CN=ChangeMe/emailAddress=admin@quantsregent.com
                serial:7F:70:AE:75:A1:E0:19:50:59:80:E7:69:CF:3B:BC:83:2D:29:D8:4A

No Revoked Certificates.
    Signature Algorithm: sha512WithRSAEncryption
         c4:5f:fd:78:4e:c6:d6:7f:55:c1:04:4d:b4:9e:e0:5e:19:90:
         eb:72:7c:94:3f:00:78:93:78:7f:28:3d:34:a7:19:53:15:79:
         40:06:9e:33:cb:ca:a7:9c:66:65:d9:d1:28:a0:db:78:43:10:
         5f:4a:b7:94:94:c9:60:f3:ff:f0:78:33:a9:18:ba:b3:48:42:
         b0:59:24:9b:41:47:10:71:89:ac:ef:50:df:a1:dd:8e:16:f6:
         a1:db:10:94:37:3b:f8:ad:67:99:0b:89:3f:67:04:2a:71:3f:
         8d:4c:55:f1:b1:23:45:12:96:d4:1c:7d:1b:1c:ce:50:79:be:
         98:25:8f:1c:91:f7:1f:39:20:ac:30:9c:21:a7:3d:79:ff:0e:
         5a:fc:f3:d7:20:92:5b:a0:9a:90:de:c1:d1:f1:29:2d:43:a6:
         67:74:f0:f0:05:0f:b8:40:7b:74:76:b7:e1:ca:16:4f:7e:62:
         66:5c:58:7f:f8:5f:8e:e9:1b:fe:de:25:6e:3d:86:d3:c6:c7:
         30:f1:04:13:c4:da:3a:a2:be:10:82:cd:4c:b8:dc:98:54:0c:
         31:4a:76:b0:21:2d:2d:6d:6f:31:ea:91:ea:3c:2d:ca:93:61:
         87:82:49:d8:4f:1b:74:74:0f:75:bc:77:bc:55:df:5d:4a:69:
         4c:ff:75:ef
```

Following errors will appear in the logs.

```
Jun  3 18:53:29 mail ovpn-taskmaster[384901]: 150.249.168.51:1194 VERIFY ERROR: depth=0, error=CRL has expired: C=JP, ST=CB, L=Ainokawa, O=quantsregent, OU=My Organizational Unit, CN=worker, emailAddress=admin@quantsregent.com
```

Puppet can renew the crl. To do so set `rl_auto_renew: true` in the server configuration.
However, since the schedule range is hardcoded to 1-4 in the openvpn module, you
need to run puppet during that time for the renewal to work. Renew period and repeat
are set to `monthly` and 2 by default. This means the renew will happen twice every
month.

```
       if ($crl_auto_renew) {
         schedule { "renew crl.pem schedule on ${name}":
           range  => '0 - 23',
           period => $crl_renew_schedule_period,
           repeat => $crl_renew_schedule_repeat,
         }
```

You will see the following puppet log when this happens.

```
Notice: /Stage[main]/Qr_openvpn::Server/Openvpn::Server[taskmaster]/Exec[renew crl.pem on taskmaster]/returns: executed successfully
```



